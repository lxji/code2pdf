package com.kuaihuoyun.toolkit.code2pdf;

import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.Executors;

/**
 * Created by lxji on 2017/3/2.
 */
public class MainFrame extends JFrame {
    private static final Logger log = LoggerFactory.getLogger(MainFrame.class);

    private JProgressBar progressBar;
    private JLabel codePathLabel;
    private JLabel outPathLabel;
    private JLabel progressLabel;

    private CodeToPdfWriter codeToPdfWriter = new CodeToPdfWriter();

    public MainFrame() {
        this.setTitle("Code to Pdf");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.setMinimumSize(new Dimension(300, 100));
        this.setMaximumSize(new Dimension(300, 100));
        this.setSize(new Dimension(300, 100));
        this.setPreferredSize(new Dimension(300, 100));
        initComponents();

        this.pack();
        this.setLocationRelativeTo(null);
    }

    private void initComponents() {
        JPanel choosePanel = new JPanel();
        add(choosePanel, BorderLayout.NORTH);
        choosePanel.setLayout(new GridLayout());
        choosePanel.add(new JLabel("选择代码根目录:"));
        JButton chooseRootBtn = new JButton("打开...");
        choosePanel.add(chooseRootBtn);
        chooseRootBtn.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int op = fileChooser.showOpenDialog(MainFrame.this);
            if (op == JFileChooser.APPROVE_OPTION) {
                prepareCodePath(fileChooser.getSelectedFile());
            }
        });

        JPanel filePath = new JPanel();
        filePath.setLayout(new BorderLayout());
        add(filePath, BorderLayout.CENTER);
        JPanel codePathPanel = new JPanel();
        codePathPanel.setLayout(new GridLayout());
        filePath.add(codePathPanel, BorderLayout.NORTH);
        codePathPanel.add(new JLabel("代码目录:"));
        codePathLabel = new JLabel();
        codePathPanel.add(codePathLabel);

        JPanel outPathPanel = new JPanel();
        outPathPanel.setLayout(new GridLayout());
        filePath.add(outPathPanel, BorderLayout.CENTER);
        outPathPanel.add(new JLabel("输出目录:"));
        outPathLabel = new JLabel();
        outPathPanel.add(outPathLabel);

        JPanel progressPanel = new JPanel();
        progressPanel.setLayout(new GridLayout());
        add(progressPanel, BorderLayout.SOUTH);
        progressPanel.add(new JLabel("进度:"));

        JPanel progressBarPanel = new JPanel();
        progressBarPanel.setLayout(new GridLayout());
        progressPanel.add(progressBarPanel);
        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(100);
        progressBar.setMinimumSize(new Dimension(100, 20));
        progressBar.setSize(100, 20);
        progressBar.setPreferredSize(new Dimension(100, 20));
        progressBarPanel.add(progressBar);
        progressLabel = new JLabel("0%");
        progressBarPanel.add(progressLabel);
    }

    private void prepareCodePath(File codePath) {
        log.info("prepareCodePath:{}", codePath.getAbsolutePath());

        codePathLabel.setText(codePath.getAbsolutePath());

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int op = fileChooser.showSaveDialog(this);
        if (op == JFileChooser.APPROVE_OPTION) {
            startConvert(codePath, fileChooser.getSelectedFile());
        }

    }

    private void startConvert(File codePath, File outPath) {
        log.info("startConvert:{}, outPath:{}", codePath, outPath);

        progressBar.setValue(0);
        outPathLabel.setText(outPath.getAbsolutePath());

        Executors.newSingleThreadExecutor().execute(()-> {
            try {
                codeToPdfWriter.write(codePath, outPath, new ProgressListener() {
                    @Override
                    public void onUpdate(int progress) {
                        updateProgress(progress);
                    }

                    @Override
                    public void onComplete() {
                        updateProgress(100);
                    }

                    @Override
                    public void onException() {
                        reset();
                    }
                });
            } catch (FileNotFoundException e) {
                log.error("error:{}", e);
                reset();
            } catch (DocumentException e) {
                log.error("error:{}", e);
                reset();
            }
        });
    }

    private void reset() {
        updateProgress(0);
        codePathLabel.setText("");
        outPathLabel.setText("");
    }

    private void updateProgress(int value) {
        progressBar.setValue(value);
        progressLabel.setText(value + "%");
    }
}
