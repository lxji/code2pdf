package com.kuaihuoyun.toolkit.code2pdf;

/**
 * Created by lxji on 2017/3/2.
 */
public interface ProgressListener {
    void onUpdate(int progress);
    void onComplete();
    void onException();
}
