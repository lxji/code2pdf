package com.kuaihuoyun.toolkit.code2pdf;

import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

/**
 * Created by lxji on 2017/3/2.
 */
public class Launcher {
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        new MainFrame().setVisible(true);
    }
}
